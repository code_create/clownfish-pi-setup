#!/bin/bash
apt-get update
apt-get upgrade -y
apt-get install -y --no-install-recommends xserver-xorg x11-xserver-utils xinit openbox
apt-get install -y --no-install-recommends chromium-browser
mv -f autostart /etc/xdg/openbox/autostart
mv -f environment /etc/xdg/openbox/environment
echo "[[ -z \$DISPLAY && \$XDG_VTNR -eq 1 ]] && startx -- -nocursor" >> ~/.bash_profile